//
//  BestsellersViewController.swift
//  BooksApp
//
//  Created by Daniyal on 09.03.2021.
//

import UIKit
import Firebase

class BestsellersViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    private var currentPage: Int = 1
    private var booksArray: [BookEntity] = []
    private var imageName: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Best sellers"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: BestsellerBookCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: BestsellerBookCell.identifier)
        retrieveBooks()
    }
    
}

    
extension BestsellersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let bookInfoVC = storyboard.instantiateViewController(withIdentifier: "BookInfoViewController") as? BookInfoViewController {
            navigationController?.pushViewController(bookInfoVC, animated: true)
            bookInfoVC.dataID = booksArray[indexPath.row].id
            bookInfoVC.title = booksArray[indexPath.row].bookName
        }
    }
}


extension BestsellersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(booksArray.count)
        return booksArray.count
    }
    
    func retrieveBooks() {
        let dbReference = Database.database().reference().child("Books")
        dbReference.observe(.childAdded) { (snapshot) in
            let snapshotValue = snapshot.value as? [String: String]
            guard let id = snapshotValue?["id"] else { return }
            guard let bookName = snapshotValue?["book_name"] else { return }
            guard let imageName = snapshotValue?["image_name"] else { return }
            self.booksArray.append(BookEntity(id: id, bookName: bookName, imageName: imageName))
//            print(self.booksArray)
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BestsellerBookCell.identifier, for: indexPath) as! BestsellerBookCell
        let storageReference = Storage.storage().reference(withPath: "images/\(self.booksArray[indexPath.row].imageName)")
            storageReference.getData(maxSize: 4 * 1024 * 1024) { (data, error) in
                if error != nil {
                    print("Error downloading!")
            }
                if let data = data {
                    cell.bookImageView.contentMode = .scaleAspectFill
                    cell.bookImageView.clipsToBounds = true
                    cell.bookImageView.image = UIImage(data: data )
                }
            }
        cell.bookNameLabel.text = booksArray[indexPath.row].bookName
        return cell
    }
    
}
