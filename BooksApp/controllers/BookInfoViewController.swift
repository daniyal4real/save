//
//  BookInfoViewController.swift
//  BooksApp
//
//  Created by Daniyal on 13.03.2021.
//

import UIKit
import Firebase


class BookInfoViewController: UIViewController {
    
    private var bookArray: [BookEntity] = []
    var dataID: String = ""
    private var imageName: String = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: BookInfoCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: BookInfoCell.identifier)
        retriveBookInfo()
    }
    
    
    func retriveBookInfo() {
        let dbReference = Database.database().reference().child("Books").child(dataID)
        dbReference.observe(.value) { (snapshot) in
            let snapshotValue = snapshot.value as? [String: String]
            guard let id = snapshotValue?["id"] else { return }
            guard let bookName = snapshotValue?["book_name"] else { return }
            guard let imageName = snapshotValue?["image_name"] else { return }
            self.imageName = imageName
            self.bookArray.append(BookEntity(id: id, bookName: bookName, imageName: self.imageName))
            self.tableView.reloadData()
            print(self.bookArray)
    }
  }
    
}



extension BookInfoViewController: UITableViewDelegate
{
    
}

extension BookInfoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: BookInfoCell.identifier, for: indexPath) as! BookInfoCell
            let storageReference = Storage.storage().reference(withPath: "images/\(self.imageName)")
                    storageReference.getData(maxSize: 4 * 1024 * 1024) { (data, error) in
                        if error != nil {
                            print("Error downloading!")
                    }
                        if let data = data {
                            cell.bookImageView.contentMode = .scaleAspectFill
                            cell.bookImageView.clipsToBounds = true
                            cell.bookImageView.image = UIImage(data: data )
                        }
                    }
            cell.bookNameLabel.text = bookArray[indexPath.row].bookName
            cell.bookDescriptionLabel.text = bookArray[indexPath.row].bookName
            return cell
    }
}
