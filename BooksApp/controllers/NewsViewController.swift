//
//  NewsViewController.swift
//  BooksApp
//
//  Created by Daniyal on 04.03.2021.
//

import UIKit
import iCarousel
import Firebase

class NewsViewController: UIViewController, iCarouselDataSource {

    
    

    @IBOutlet weak var bookView: UIView!
    private var booksArray: [BookEntity] = []
    private var imageNames: [String] = []
    private var imageName: String = ""
    
    let bookCarousel: iCarousel = {
        let view = iCarousel()
        view.type = .rotary
        return view
    }()
    
    let bookCarousel2: iCarousel = {
        let view2 = iCarousel()
        view2.type = .cylinder
        return view2
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(bookCarousel)
        view.addSubview(bookCarousel2)
        bookCarousel.dataSource = self
        bookCarousel.autoscroll = -0.3
        bookCarousel2.dataSource = self
        
        bookCarousel.frame = CGRect(x: 80, y: 100, width: 230, height: 380)
        bookCarousel2.frame = CGRect(x: 0, y: 400, width: 230, height: 380)
        retrieveBooks()
    }
    
    func retrieveBooks() {
        let dbReference = Database.database().reference().child("Books")
        dbReference.observe(.childAdded) { (snapshot) in
            let snapshotValue = snapshot.value as? [String: String]
            guard let bookName = snapshotValue?["book_name"] else { return }
            guard let imageName = snapshotValue?["image_name"] else { return }
            guard let id = snapshotValue?["id"] else { return }
            self.booksArray.append(BookEntity(id: id, bookName: bookName, imageName: imageName))
            self.imageNames.append(bookName)
//            print(self.imageNames)
        }
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return 10
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        retrieveBooks()
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 250))
        view.backgroundColor = .blue

        let imageView = UIImageView(frame: view.bounds)
        view.addSubview(imageView)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
//        let element = self.imageNames.randomElement()
//        print(element as Any)
//        print(self.imageNames)

        let storageReference = Storage.storage().reference(withPath: "images/7 habits.jpg")
            storageReference.getData(maxSize: 4 * 1024 * 1024) { (data, error) in
                if error != nil {
                    print("Error downloading!")
            }
                if let data = data {
                    imageView.image = UIImage(data: data)
            }
        }
        return view
    }

}
