//
//  ViewController.swift
//  BooksApp
//
//  Created by Daniyal on 03.03.2021.
//

import UIKit
import Firebase

class LogInViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        logInButton.layer.cornerRadius = 8
        logInButton.layer.masksToBounds = true 
    }

    @IBAction func logInButtonPressed(_ sender: Any) {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, err) in
            if err != nil {
                print(err!)
            } else {
                print("Logged in successfully")
                self.performSegue(withIdentifier: "logInToBar", sender: self)
            }
        }
    }
    
}

