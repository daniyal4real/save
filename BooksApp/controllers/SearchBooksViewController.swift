//
//  ReviewsViewController.swift
//  BooksApp
//
//  Created by Daniyal on 13.03.2021.
//

import UIKit
import Firebase

class SearchBooksViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    private var bookNameArray = [String]()
    private var searchBook = [String]()
    private var booksArray: [BookEntity] = []
    private var searching: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        tableView.register(UINib(nibName: BookNameCell.identifier, bundle: Bundle.main), forCellReuseIdentifier:  BookNameCell.identifier)
        retrieveBooks()
    }
    
    
    func retrieveBooks() {
        let dbReference = Database.database().reference().child("Books")
        dbReference.observe(.childAdded) { (snapshot) in
            let snapshotValue = snapshot.value as? [String: String]
            guard let bookName = snapshotValue?["book_name"] else { return }
            guard let imageName = snapshotValue?["image_name"] else { return }
            guard let id = snapshotValue?["id"] else { return }
            self.booksArray.append(BookEntity(id: id, bookName: bookName, imageName: imageName))
            self.bookNameArray.append(bookName)
            self.tableView.reloadData()
        }
    }

}

extension SearchBooksViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let bookInfoVC = storyboard.instantiateViewController(withIdentifier: "BookInfoViewController") as? BookInfoViewController {
            navigationController?.pushViewController(bookInfoVC, animated: true)
            bookInfoVC.dataID = booksArray[indexPath.row].id
            bookInfoVC.title = booksArray[indexPath.row].bookName
        }
    }
}


extension SearchBooksViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return self.searchBook.count
        } else {
            return self.bookNameArray.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookNameCell.identifier, for: indexPath) as! BookNameCell
        
        if searching {
            cell.bookNameLabel.text = searchBook[indexPath.row]
        } else {
            cell.bookNameLabel.text = bookNameArray[indexPath.row]
        }
        
        return cell
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchBar.delegate = self
        self.searchBook = bookNameArray.filter({$0.lowercased().prefix(searchText.count) == searchText.lowercased() })
        self.searching = true
        self.tableView.reloadData()
    }
}

