//
//  BookInfoImageCell.swift
//  BooksApp
//
//  Created by Daniyal on 13.03.2021.
//

import UIKit

class BookInfoCell: UITableViewCell {
    
    public static let identifier: String = "BookInfoImageCell"
    
    @IBOutlet weak var bookImageView: UIImageView!
    @IBOutlet weak var bookNameLabel: UILabel!
    @IBOutlet weak var bookDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bookImageView.layer.cornerRadius = 8
        bookImageView.layer.masksToBounds = true
        bookImageView.backgroundColor = .blue
        
        bookImageView.contentMode = .scaleAspectFit
        bookImageView.clipsToBounds = true
    }

}
