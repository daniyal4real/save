//
//  BookNameCell.swift
//  BooksApp
//
//  Created by Daniyal on 13.03.2021.
//

import UIKit

class BookNameCell: UITableViewCell {

    public static let identifier: String = "BookNameCell"
    
    @IBOutlet weak var bookNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}
