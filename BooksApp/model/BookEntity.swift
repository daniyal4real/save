//
//  BookEntity.swift
//  BooksApp
//
//  Created by Daniyal on 11.03.2021.
//

import Foundation


struct BookEntity {
    let id: String
    let bookName: String
    let imageName: String
    
    
    enum CodingKeys: String, CodingKey {
        case id 
        case bookName = "book_name"
        case imageName = "image_name"
    }
}
